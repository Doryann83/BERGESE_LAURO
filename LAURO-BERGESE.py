from flask import Flask, jsonify
from multiprocessing import Value
from raven.contrib.flask import Sentry
app = Flask(__name__)

sentry = Sentry(app, dsn='https://a1a30cb59684462993d47054846dbdd4:5f903562f85743119eca2f9c9d729b0c@sentry.io/257545')
sentry.captureMessage('Demmarage de l application')

compteur = Value('i', 0)

@app.route('/')
def main():
    return "Projet Docker de Sebastien BERGESE et Doryann LAURO"


@app.route('/ping')
def ping():
    sentry.captureMessage('La valeur pong a été compté une fois')
    with compteur.get_lock():
        compteur.value += 1
    return jsonify(ping='pong')


@app.route('/count')
def count():
    sentry.captureMessage('On a compté le total des pongs')
    return jsonify(total=compteur.value)


if __name__ == "__main__":
    app.run()

